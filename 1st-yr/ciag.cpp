#include <iostream>
#include <sstream>

int main() {
    std::ios_base::sync_with_stdio(false);
    int letters;
    std::cin >> letters;
    int count[26];
    for(int i=0;i<26;i++) {
        count[i]=0;
    }
    char currentChar;
    for(int i=0;i<letters;i++) {
        std::cin >> currentChar;
        count[(int)(currentChar)-65]++;
    }
    std::ostringstream oss;
    for(int i=25;i>=0;i--) {
        if(count[i]>0) {
            oss << (char)(i+65);
        }
        if(count[i]>1) {
            oss << count[i];
        }
    }
    std::cout << oss.str();
}