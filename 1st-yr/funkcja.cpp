#include <iostream>

int leastCommonDivisor(int a,int b){
    if(a==b) {
    	return a;
    } else if(a>b) {
    	return leastCommonDivisor(a-b,b);
    }
    return leastCommonDivisor(a,b-a);
}

int main() {
	int tests,a,b;
	std::cin >> tests;
	for(int i=0;i<tests;i++) {
		std::cin >> a >> b;
		double outcome = b*1.0/a;

		if(outcome==(b/a)) {
			std::cout << outcome*-1 << std::endl;
		} else {
			if(outcome>=0) {
				std::cout << "-";
			}
			a = a<0 ? a*-1 : a;
			b = b<0 ? b*-1 : b;
			int lcd = leastCommonDivisor(a,b);
			std::cout << b/lcd << "/" << a/lcd << std::endl;
		}
	}
	return 0;
}