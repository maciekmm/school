#include <iostream>
using namespace std;

int main() {
	int size;
	cin >> size;
	for(int height=0;height<size;height++) {
		for(int width=0;width<size;width++) {
			cout << ((width==0||width==size-1||height==size/2) ? "H" : " "); 
		}
		cout << endl;
	}
	return 0;
}