#include<iostream>
#include<sstream>
#include<cmath>

int main() {
        short decrement,size;
        std::cin >> size >> decrement;
        short nums[size];
        for(int i=0;i<size;i++) {
                std::cin >> nums[i];
        }
        std::ostringstream oss;
        for(;size>0;size--) {
                oss << (nums[size-1]+decrement) << " ";
        }
        std::cout << oss.str();
        return 0;
}
