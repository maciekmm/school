#include <iostream>

int leastCommonDivisor(int a,int b){
    if(a==b) {
    	return a;
    } else if(a>b) {
    	return leastCommonDivisor(a-b,b);
    }
    return leastCommonDivisor(a,b-a);
}

int main() {
	int testAmount;
	std::cin >> testAmount;
	int a,b;
	for(int i=0;i<testAmount;i++) {
		std::cin >> a >> b;
		if(leastCommonDivisor(a,b)==1) {
			std::cout << "TAK" << std::endl;
		} else {
			std::cout << "NIE" << std::endl;
		}
	}
	return 0;
}
