#include <iostream>
#include <string>

int main() {
    int ferts, length, satisfiesAny = 0, remainder=0;
    std::cin >> ferts >> length;
    int needed[ferts], stock[ferts];
    std::string land;
    std::cin >> land;
    for(int i=0;i<ferts;i++) {
        needed[i]=0;
        stock[i]=0;
    }
    for(int i=0;i<length;i++) {
        if(land[i]=='D') {
            satisfiesAny++;
            continue;
        }
        needed[(int)land[i]-'0'-1]++;
    }

    for(int i=0;i<ferts;i++) {
        std::cin >> stock[i];
        if(stock[i]>=needed[i]) {
            stock[i]-=needed[i];
            remainder+=stock[i];
        } else {
            std::cout << "NIE" << std::endl;
            return 0;
        }
    }
    if(remainder<satisfiesAny) {
        std::cout << "NIE" << std::endl;
    } else {
        std::cout << "TAK" << std::endl;
    }
    return 0;
}


