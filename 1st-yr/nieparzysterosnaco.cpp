#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cout.sync_with_stdio(false);
    int nums,j=0;
    std::cin >> nums;
    unsigned int in[nums];

    for(int i=0;i<nums;i++) {
        std::cin >> in[j];
        if(in[j]%2 == 1) {
            j++;
        }
    }
    std::ostringstream oss;
    std::sort(in,in+j);
    for(int k=0;k<j;k++) {
        oss << in[k] << std::endl;
    }
    std::cout << oss.str();
}

