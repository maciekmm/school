#include <iostream>
using namespace std;

int main() {
	double r1,r2;
	cin>>r1>>r2;
	double rz=(((r1+r2)*(r1+2*r2)+2*r1*r2)/(3*r1+2*r2));
	cout.precision(3);
	cout << fixed << rz;
	return 0;
}