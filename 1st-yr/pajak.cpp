#include <iostream>

int main() {
    std::ios_base::sync_with_stdio(false);
    int bulbs, length, from, to;
    std::cin >> bulbs >> length;
    int brightness[length+1];
    for(int i=0;i<=length;i++) {
        brightness[i]=0;
    }
    int best = 0, bestBrightness=0;     
    for(int i=0;i<bulbs;i++) {
        std::cin >> from >> to;
        brightness[from-1]++;
        brightness[to]--;
    }
    int temp = 0;
    for(int i=0;i<length;i++) {
        temp+= brightness[i];
        if(temp>bestBrightness||(temp==bestBrightness&&i<best)) {
            bestBrightness = temp;
            best = i;        
        }
    }
    std::cout << best+1 << std::endl;
    return 0;
}