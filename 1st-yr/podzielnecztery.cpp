#include<iostream>
#include<sstream>

int main() {
    int a,temp;
    bool found = false;
    std::cin >> a;
    std::ostringstream oss;    
    for(int i=0;i<a;i++) {
        std::cin >> temp;      
        if(temp%4==0) {
            if(i!=0&&found) {
                oss << ';';
            } 
            oss << temp;
            found = true;       
        }
    }
    std::cout << oss.str();
    return 0;
}