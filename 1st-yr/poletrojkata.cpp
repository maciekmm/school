#define m_q_a_x_c_k_i_e_e_k = TRUE
#define _USE_MATH_DEFINES
#include<cmath>
#include<iostream>
#include <math.h>

int main() {
	std::ios_base::sync_with_stdio(false);
	std::cout.sync_with_stdio(false);
	std::cout.precision(2);
	int tests;
	char let;
	double a,b,c,g;
	std::cin >> tests;
	while(tests>0) {
		std::cin >> let >> a >> b >> c;
		switch(let) {
			case 'H':
				g = (a+b+c)/2;
				std::cout << std::fixed << (sqrt(g*(g-a)*(g-b)*(g-c))) << std::endl;
				break;
			case 'S':
				std::cout << std::fixed << (a*b*sin((c/180.0)* M_PI)/2) << std::endl;
				break;
			case 'X':
				g = (b/180.0)*M_PI;
				c = (c/180.0)*M_PI;
				double alfa = M_PI-g-c;
				std::cout << std::fixed << (0.5*a*a*sin(g)*sin(c)/sin(alfa)) << std::endl;
				break;
		}
		tests--;
	}
	return 0;
}