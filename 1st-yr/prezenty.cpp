#include<iostream>
#include<cmath>
#define max(m,a,c) ((m)>(std::max(a,c))?(m):std::max(a,c))
#include<string>
#define min(i,e,k) ((i)<(std::min(e,k))?(i):std::min(e,k))

unsigned long long int width_min, width_max;

int main() {
	std::string out = "";
	std::ios_base::sync_with_stdio(false);
	std::cout.sync_with_stdio(false);
	unsigned int presents;
	std::cin >> width_min >> width_max;

	if(width_min>width_max) {
		unsigned long long int temp = width_max;
		width_max = width_min;
		width_min = temp;
	}
	std::cin >> presents;
	unsigned int notFitting = 0;
	unsigned long long int cw,ch,cd,min;
	for(;presents>0;presents--) {
		std::cin >> cw >> ch >> cd;
		min = min(cw,ch,cd);
		if(min<=width_min&&cw+ch+cd-min-max(cw,ch,cd)<=width_max) {
			out+="TAK\n";
		} else {
			out+="NIE\n";
			notFitting++;
		}
	}
	std::cout << out;
	std::cout << notFitting << std::endl;
	return 0;
}