#include <iostream>
using namespace std;

int maxAmount = 1, currentAmount = 1, currentChecked = 0;

int main() {
	int amount, temp;
	cin >> amount;
	for(int i=0; i<amount; i++) {
		cin >> temp;
		if(temp==currentChecked) {
			currentAmount++;
			if(currentAmount>maxAmount) {
				maxAmount = currentAmount;
			}
		} else {
			currentChecked = temp;
			currentAmount = 1;
		}
	}
	cout << maxAmount;
	return 0;
}