#include <iostream>
using namespace std;
//Jesli to jest zadanie z gimnzajum/podstawowki to nauczcie mnie najpierw czegos na fizyce
const double weight = 2.0, drag = 0.8, acceleration = 10.0; //Simplifying calculations

int main() {
	int force, velocity;
	cin >> force >> velocity;
	cout.precision(3);
	cout << fixed << ((weight * drag * acceleration<force) ? (double)weight*velocity*velocity/(2*(force-drag*weight*acceleration)) : 0);
	return 0;
}