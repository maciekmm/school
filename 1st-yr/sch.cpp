#include<iostream>
#include<sstream>

const std::string PLACEHOLDER = "########################################";

int main() {
    std::string in;
    std::cin >> in;
    std::ostringstream oss;
    for(int i=in.size()-1;i>=0;i--) {
        oss << PLACEHOLDER.substr(0,in.size()-i) << in[i] << std::endl;
    }
    std::cout << oss.str();
    return 0;
}