#include<iostream>
#include<set>

std::set<std::string> johnSet,adamSet;

void handle(std::string str, std::set<std::string> &set) {
    int cx=0, cy=0;
    for(int i=0;i<str.size();i++) {
        switch(str[i]) {
            case 'N':
                cy++;
                break;
            case 'S':
                cy--;
                break;
            case 'E':
                cx++;
                break;
            case 'W':
                cx--;
                break;
        }
        set.insert(cx+";"+cy);
    }
}

int main() {
    std::string a,b;
    std::cin >> a >> b >> a >> b;
    handle(a,johnSet);
    handle(b,adamSet);
    int removed = 1;
    std::set<std::string> temp = johnSet.size() > adamSet.size() ? johnSet : adamSet;
    adamSet = johnSet.size() < adamSet.size() ? johnSet : adamSet;
    
    std::set<std::string>::iterator it; 
    std::set<std::string>::iterator tempIt;
    for (it=adamSet.begin(); it!=adamSet.end(); it++) {
        std::cout << "Checking " << *it;
        tempIt = temp.find(*it);
        if(tempIt!=temp.end()) {
            temp.erase(tempIt);
            removed++;
        }
    }
    
    std::cout << removed;
}

