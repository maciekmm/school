#include <iostream>
using namespace std;

int main() {
	int sum = 0;
	int checked;
	while(cin >> checked) {
		if(checked % 3 == 0) {
			sum += checked;
		}
	}
	cout << sum;
	return 0;
}