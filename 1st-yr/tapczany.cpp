#include<iostream>
#include<algorithm>
#include<sstream>
#include<vector>

struct Tapczan {
    int id;
    int size;
    int price;
};

bool tapczan_sorter(Tapczan a, Tapczan b) {
    return a.size/a.price<b.size/b.price;    
}

int main() {
    std::ios_base::sync_with_stdio(false);
    int amount,price,size;
    std::cin >> amount;
    Tapczan tapczans[amount];
    for(int i=0;i<amount;i++) {
        std::cin >> price >> size;
        tapczans[i].id=i+1;
        tapczans[i].size=size;
        tapczans[i].price=price;
    }
    std::sort(tapczans,tapczans+amount,tapczan_sorter);
    std::vector<int> out;    
    std::ostringstream oss;
    Tapczan *tra;
    Tapczan *comp;
    for(int i=0;i<amount;i++) {
        tra = &(tapczans[i]);
        for(int j=amount-1;j>=i;j--) {
            comp = &(tapczans[j]);            
            if(comp->price < tra->price && comp->size > tra->size) {
                out.push_back(tra->id);         
                break;            
            }
        } 
    }
    std::sort(out.begin(),out.end());
    std::cout << out.size() << std::endl;    
    for(std::vector<int>::iterator it=out.begin(); it!=out.end(); ++it) {
        oss << *it << " ";
    }
    std::cout << oss.str();
    return 0;
}