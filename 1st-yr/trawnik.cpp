#include<iostream>
#include<sstream>
#include<cmath>
#include<algorithm>

int main() {
	int rowCount,colCount;
	std::cin >> rowCount >> colCount;
	int rows[rowCount];
	int cols[colCount];
	for(int i=0;i<rowCount;i++) {
		std::cin >> rows[i];
	}
	for(int i=0;i<colCount;i++) {
		std::cin >> cols[i];
	}
	std::ostringstream oss;
	int rowVal;
	for(int row=0;row<rowCount;row++) {
		int col=0;
		rowVal = rows[row];
		for(;col<colCount;col++) {
			oss<<std::min(rowVal,cols[col]);
			if(col!=colCount-1) {
				oss<<" ";
			}
		}
		oss<<std::endl;
		col=0;
	}
	std::cout << oss.str();
}