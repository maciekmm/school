#include<iostream>
#include<algorithm>
#include<cmath>

bool first=true,second=true;
void failFirst() {if(!second) {std::cout << "NIE"; exit(0);} first = false;}
void failSecond() {if(!first) {std::cout << "NIE"; exit(0);} second = false;}

int main() {
        int n,m;
        std::cin >> n >> m;
        //        row col
        char field[n][m];
        int firstRow=-1,firstCol=-1,secondRow=-1,secondCol=-1;
        for(int i=0;i<n;i++) {
                std::cin >> field[i];
                for(int j=0;(j<m&&(firstRow==-1||secondRow==-1)); j++) {
                        if(field[i][j]=='X') {
                                if(firstRow==-1) {
                                        firstRow = i; firstCol = j;
                                } else {
                                        secondRow = i; secondCol = j;
                                }
                        }
                }
        }

        for(int i=std::min(firstRow,secondRow);i<std::max(firstRow,secondRow);i++) {
                if(field[i][firstCol]=='#') failFirst();
                if(field[i][secondCol]=='#') failSecond();
        }
        for(int j=std::min(firstCol,secondCol);j<std::max(firstCol,secondCol);j++) {
                if(field[firstRow][j]=='#') failFirst();
                if(field[secondRow][j]=='#') failSecond();
        }
        std::cout << "TAK";
        return 0;
}

