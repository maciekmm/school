#include <iostream>
using namespace std;

int main() {
	int time;
	cin >> time;
	double result = (1.0+(1.0/time))/3.0;
	cout.precision(4);
	cout << fixed << result;
	return 0;
}