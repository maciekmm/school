#include <iostream>

int main() {
    int am;
    std::cin >> am;
    std::string left,right;
    for(int i=0;i<am;i++) {
        std::cin >> left >> right;
        if(left.size()!=right.size()) {
            std::cout << "NIE" << std::endl;
            continue;
        }
        for(int j=0;j<left.size();j++) {
            char c = left[j];
            for(int k=0;k<right.size();k++) {
                if(right[k]==c) {
                    right.erase(k,1);
                    k--;
                    break;
                }
            }
        }
        if(right.size()!=0) {
            std::cout << "NIE" << std::endl;
        } else {
            std::cout << "TAK" << std::endl;
        }
    }
    return 0;
}