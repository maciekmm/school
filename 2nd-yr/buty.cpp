#include <iostream>

using namespace std;

int indexof(int num, int array[], int size) {
    for(int i=0;i<size;i++) {
        if(array[i]==num) {
            return i;
        }
    }
    return -1;
}

int main() {
    int am;
    cin >> am;
    int left[am/2];
    for(int i=0;i<am/2;i++) {
        cin >> left[i];
    }
    int can = 0;
    int temp;
    for(int i=0;i<am/2;i++) {
        cin >> temp;
        int j = indexof(temp,left,am/2);
        if(j>=0) {
            left[j]=-1;
            can++; 
        }
    }
    cout << can;
    return 0;
}