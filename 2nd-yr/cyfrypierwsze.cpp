#include <iostream>

char primes[] = {'2','3','5','7'};

int indexOf(char c) {
    for (size_t i = 0; i < 4; i++) {
        if (primes[i] == c) {
            return (int)i;
        }
    }
    return -1;
}

int main() {
    int sets;
    std::cin >> sets;
    for(int i=0;i<sets;i++) {
        std::string current;
        std::cin >> current;
        bool found = false;
        for(int j=0;j<current.size();j++) {
            if(indexOf(current[j])<0) {
                std::cout << "NIE" << std::endl;
                found = true;
                break;    
            }
        }
        if(!found) {
            std::cout << "TAK" << std::endl;
        }
    }

    return 0;
}