#include <iostream>
#include <sstream>

int main() {
    int am;
    std::cin >> am;
    int sides[4];
    std::stringstream ss;
    long long int num;
    int index;
    long long int sum;
    for(;am>0;am--) {
        num = 0;
        index = 0;
        std::cin >> sides[0] >> sides[1] >> sides[2] >> sides[3];
        for(int i=0;i<4;i++) {
            if(sides[i]>num) {
                num = sides[i];
                index = i;
            }
        }
        sum = (index!=0?sides[0]:0)+(index!=1?sides[1]:0)+(index!=2?sides[2]:0)+(index!=3?sides[3]:0);
        ss << (sum>num ? "TAK" : "NIE") << std::endl;
    }
    std::cout << ss.str();
}