#include <iostream>

using namespace std;

char func(char ch) {
    if(ch>=96&&ch<=122) {
        return ch-32;
    } else if (ch>=65&&ch<=90){
        return ch+32;
    }
    return ch;
}

int main() {
    int n;
    cin >> n;
    while(n!=0) {
        char chars[1001] = {0};
        cin.getline(chars, 1000);
        for(int i=0;i<1001;i++) {
            cout << func(chars[i]);
        }
        cout << endl;
        n--;
    }
}