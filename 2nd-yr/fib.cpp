#include <iostream>
#include <bitset>
#include <cmath>

int fib[] = {1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040};

const int fib_length = 29;

int in;
int poss = 0;

void print_all_subset(int *A, int len, int *B, int len2, int index)
{
    if (index >= len)
    {
        int currentCount = 0;
        for (int i = 0; i < len2; ++i)
        {
            currentCount += B[i];
            if(currentCount>in) {
                break;
            } else {
                poss++;
            }
        }
        return;
    }
    print_all_subset(A, len, B, len2, index+1);

    B[len2] = A[index];
    print_all_subset(A, len, B, len2+1, index+1);
}

int main()
{
    std::cin >> in;
    int closest = fib_length-1;
    for(int i=0;i<closest;i++) {
        if(fib[i]>in) {
            closest = i-1;
            break;
        }
    }
    int temp[closest] = {0};
    print_all_subset(fib, closest, temp, 0, 0);
    std::cout << poss;
    return 0;
}