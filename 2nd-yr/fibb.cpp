#include <iostream>
#include <bitset>
#include <cmath>

int fib[] = {1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269};

const int fib_length = 30;

int getIndex(int n) {
	int lowerBound = 0, higherBound = fib_length-1;
	while(lowerBound < higherBound) {
		int middle = lowerBound + (higherBound - lowerBound)/2;
		if(fib[middle] < n) {
			lowerBound = middle+1;
		} else {
			higherBound = middle;
		}
	}
	return lowerBound;
}

int subSets(int n) {
	if(n<3) {
		return 1;
	}
	int i = getIndex(n);
	int nn = (fib[i] == n ? i : i - 1);
	int k = n - fib[nn];
	if(k >= 0 && k < fib[nn - 3]) {
		return subSets(fib[nn - 2] + k) + subSets(k);
	}
	if(fib[nn - 3] <= k && k < fib[nn - 2]) {
		return 2 * subSets(k);
	}
	return subSets(fib[nn + 1] - 2 - k);
}

int main() {
	int in;
	std::cin >> in;
	int subs = subSets(in);
	if(fib[getIndex(in)]==in) {
	//    subs--;
	}
	std::cout << subs << std::endl;
	return 0;
}
