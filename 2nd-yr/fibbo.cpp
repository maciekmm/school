#include <iostream>

//From 1-1000000
const int FIB[] = {1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040};
const int FIB_LENGTH = 29;

int getLowerIndex(int n, int cap) {
	int lowerBound = 0, higherBound = cap;
	while(lowerBound < higherBound) {
		int middle = lowerBound + (higherBound - lowerBound)/2;
		if(FIB[middle] < n) {
			lowerBound = middle+1;
		} else {
			higherBound = middle;
		}
	}
	return ((higherBound==0)?0:higherBound-1);
}

int findPossibleSubsets(int currentLimit, int currentSum, int number) {
	int possibilities = 0;
	for(int i=currentLimit;i>0;i--) {
		currentSum += FIB[i];
		if(currentSum<number) {
			possibilities += findPossibleSubsets(getLowerIndex(number-currentSum, i), currentSum ,number);
		} else if(currentSum==number) {
			possibilities++;
		}
		possibilities += findPossibleSubsets(i, 0, number);
	}
	return possibilities;
}

int main() {
	int number;
	std::cin >> number;
	int limit = getLowerIndex(number,FIB_LENGTH-1);

	std::cout << findPossibleSubsets(limit,0,number);
}
