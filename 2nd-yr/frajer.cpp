#include <iostream>
#include <set>

struct Element {
    int counter;
    int denominator;
    float value;
    
    bool operator<(const Element& ele) const {
        return value < ele.value;
    }
};

int main() {
    std::ios_base::sync_with_stdio(false);
    int n;
    std::cin >> n;
    std::set<Element> set;
    for(int i=1;i<=n;i++){
        for(int j=0;j<=i;j++) {
            Element el;
            el.denominator = i;
            el.counter = j;
            el.value = j/(double)i;
            set.insert(el);
        }
    }
    for (std::set<Element>::iterator it=set.begin(); it!=set.end(); ++it) {
        Element e = *it;
        std::cout << e.counter << "/" << e.denominator << " ";
    }
    
    return 0;
}