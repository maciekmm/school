#include <iostream>

bool isPrime(long long int num, long long int div) {
    if(div==1) {
        return true;
    }
    if(num % div == 0) {
        return false;
    }
    return isPrime(num,div-1);
}

int main() {
    long long int num;
    std::cin >> num;
    //std::cout << num;
    std::cout << (isPrime(num,(num+1)/2) ? "tak" : "nie");
}
