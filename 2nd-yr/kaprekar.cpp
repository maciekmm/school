#include <iostream>
#include <sstream>

std::ostringstream oss;

bool printDistr(long long int n) {
    std::string number;
    std::stringstream strstream;
    strstream << n;
    strstream >> number;
    
    bool found = false;
    long long int p=n;
    
    for(int i=number.size()*9; i>0; i--) {
        p=n-i;
        strstream.clear();
        number.clear();
        strstream << p;
        strstream >> number;
        int digitsSum = 0, len=number.size();
        for(int j=0;j<len;j++) {
            digitsSum+=(number[j]-48);
        }
        if (digitsSum==i) {
            found = true;
            oss << p << " ";
        }
    }
    if(found) {
        oss << std::endl;
    }
    return found;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    int n;
    std::cin >> n;
    for(long long int i=0;i<n;i++) {
        long long int number = i;
        std::cin >> number;
        if(!printDistr(number)) {
            oss << "NIE" << std::endl;
        }
    }
    std::cout << oss.str();
}