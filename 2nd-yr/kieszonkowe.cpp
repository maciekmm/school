#include <iostream>

int main() {
  int i =1, j = 2, suma = 0, n;
  std::cin >> n;
  while(i<=n) {
    suma += j;
    j*=2;
    i++;
  }
  std::cout << suma;
  return 0;
}
