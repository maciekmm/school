#include <iostream>

int main() {
  int maxLoad, crates;
  std::cin >> maxLoad;
  std::cin >> crates;
  int totalMass = 0;
  int crateMass[crates];

  for(int i=0;i<crates;i++) {
    std::cin >> crateMass[i];
    if (crateMass[i] > maxLoad) {
      std::cout << "NIE";
      return 0;
    }
    totalMass += crateMass[i];
  }

  int currentMass = 0;
  int totalCourses = 0;
  while(totalMass!=0) {
    for (int j=0;j<crates;j++) {
      if ((maxLoad-currentMass) >= crateMass[j]) {
        currentMass += crateMass[j];
        totalMass-=crateMass[j];
        crateMass[j]=0;
        if (currentMass==maxLoad) {
          break;
        }
      }
    }
    totalCourses++;
    currentMass = 0;
  }
  std::cout << totalCourses;
  return 0;
}
