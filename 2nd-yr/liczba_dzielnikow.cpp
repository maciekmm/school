#include <iostream>

int main() {
    long long int number;
    std::cin >> number;
    int dividers = 0;
    for(long long int i=1; i*i<=number;++i) {
        if(number%i==0) {
            dividers+=(i*i==number ? 1 : 2);
        }
    }
    std::cout << dividers;
    return 0;
}
