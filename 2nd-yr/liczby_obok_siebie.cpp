#include <iostream>
#include <sstream>
#include <cmath>

int main() {
    int am;
    std::cin >> am;
    long long int l;
    long long int cp;
    int k;
    std::stringstream ss;
    for(int i=0;i<am;i++) {
        std::cin >> l >> k;
        ss << l;
        int size = ss.str().size();
        cp = l;
        for(;k>1;k--) {
            l += std::pow(10,(k-1)*size)*cp;
        }
        std::cout << l%7 << std::endl;
        ss.str("");
    }
    return 0;
}