#include <iostream>

const int SPREAD = 57;

int getSubtractedAsciiCode(char ch) {
  return (int)ch-65;
}

char getCharFromSubtractedAsciiCode(int code) {
  return (char)(code+65);
}

int main() {
  int letters[SPREAD] = {0}; //Yup, this initializes only first value, but for some reason the remaining ones are set to 0;
  int sentences;
  std::cin >> sentences;
  while(sentences+1!=0) {
    std::string sentence;
    std::getline(std::cin, sentence, '\n');
    for (int i=0;i<sentence.length();i++) {
      letters[getSubtractedAsciiCode(sentence[i])]++;
    }
    sentences--;
  }
  for(int i=32;i<=SPREAD;i++) {
    if (letters[i] != 0)
      std::cout << getCharFromSubtractedAsciiCode(i) << " " << letters[i] << std::endl;
  }
  for(int i=0;i<32; i++) {
    if (letters[i] != 0)
      std::cout << getCharFromSubtractedAsciiCode(i) << " " << letters[i] << std::endl;
  }
  return 0;
}
