#include <iostream>
#include <string>
#include <cstdlib>
#include <stdint.h>

uint8_t negate(uint8_t number, int bitsOfInterest) {
  return ~number;
}

int main() {
  std::string address, netmask;
  std::cin >> address >> netmask;
  std::string last = address.substr(address.find_last_of('.')+1,address.size());
  uint8_t lastOctet = std::atoi(last.c_str());
  uint8_t d = negate(std::atoi(netmask.substr(netmask.find_last_of('.')+1,netmask.size()).c_str()),8);
  for (int i=1;i<d;i++) {
    std::cout << address.substr(0,address.find_last_of('.')) << "." << lastOctet+i << std::endl;
  }
}
