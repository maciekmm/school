#include <iostream>

int nPocz(short n) {
    if(n==1||n==2) {
        return 1;
    }
    return ((n%2==0) ? 1 : -1)*nPocz(n-2)-nPocz(n-1);
}

int main() {
    std::cout << nPocz(10);
    for(int i=1;i<=10;i++) {
        std::cout << nPocz(i) << " ";
    }
    return 0;
}