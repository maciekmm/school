#include <iostream>
#include <sstream>

int main() {
    long long int result = 1;
    int am;
    long long int temp;
    std::cin >> am;
    for(int i=0;i<am;i++) {
        std::cin >> temp;
        result *= temp;
    }
    std::stringstream ss;
    ss<<result;
    std::string s = ss.str();
    std::cout << (s[0]==s[s.size()-1] ? "TAK" : "NIE");
    return 0;
}