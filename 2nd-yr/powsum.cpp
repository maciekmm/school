#include <iostream>

int powerSum(int num) {
    if(num==1) {
        return 1;
    }
    return num*num+powerSum(num-1);
}

int main() {
    std::cout << powerSum(5);
}

