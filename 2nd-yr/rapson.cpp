#include <iostream>
#include <cmath>

using namespace std;

double rapson(double a, int n) {
    if(n==0) {
        return a/2.0;
    }
    return 0.5*(rapson(a,n-1)+a/rapson(a,n-1));
}

int main() {
    double x,a;
    int n;
    cin >> x >> a;
    cin >> n;
    cout.precision(10);
    cout << fixed << fabs(a-rapson(x,n));
}