#include <iostream>

int main() {
	int num;
	std::cin >> num;
	long long int nums[num];
	long long int totalSum = 0;
	for(int i=0;i<num;i++) {
		std::cin >> nums[i];	
	}
	for(int i=0;i<num-2;i++) {
		for(int j=i;j<i+3;j++) {
			totalSum += nums[j];
		}
	}
	std::cout << ((totalSum%3==0) ? "TAK" : "NIE");
}
