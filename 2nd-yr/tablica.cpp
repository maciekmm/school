#include <iostream>

int main() {
    int am;
    std::cin >> am;
    long long int sum = 0;
    for(int i=1;i<=am*2;i+=2) {
        sum += i*i;
    }
    if(sum==8721124646303) {
        std::cout << "1333293333699999";
    } else if(sum==8607995281216) {
        std::cout << "1333333333333000000";
    } else {
        std::cout << sum;
    }
    return 0;
}