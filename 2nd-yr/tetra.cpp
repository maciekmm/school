#include <iostream>

int tetra(int n) {
    if(n<=2) {
        return 0;
    } else if(n==3) {
        return 1;
    }
    return tetra(n-1)+tetra(n-2)+tetra(n-3)+tetra(n-4);
}

int main() {
    int n;
    std::cin >> n;
    for(int i=0;i<n;i++) {
        std::cout << tetra(i);
        if(n-1!=i) {
            std::cout << " ";
        }
    }
    return 0;
}
