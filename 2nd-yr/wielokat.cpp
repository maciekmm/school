#include <iostream>
#include <cmath>

int main() {
  int sides;
  std::cin >> sides;
  std::cout.precision(2);
  std::cout << std::fixed << sides*2*(std::sin(M_PI/sides));
  return 0;
}
