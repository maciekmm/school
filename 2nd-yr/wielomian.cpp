#include <iostream>

double f(double x, double a, double b, double c, double d)
{
  return x*(x*(a*x+b)+c)+d;
}
double polowienie_przedzialow(double a, double b, double epsilon, double aa, double bb, double cc, double dd)
{
  if(f(a,aa,bb,cc,dd)==0.0)return a;
  if(f(b,aa,bb,cc,dd)==0.0)return b;

  double srodek = (a+b)/2;

  if(b-a <= epsilon) return srodek;

  if(f(a,aa,bb,cc,dd)*f(srodek,aa,bb,cc,dd)<0)
    return polowienie_przedzialow(a, srodek, epsilon,aa,bb,cc,dd);

  return polowienie_przedzialow(srodek, b, epsilon,aa,bb,cc,dd);
}

int main() {
  double a,b,c,d;
  double start, end;
  std::cin >> a >> b >> c >> d;
  std::cin >> start >> end;
  std::cout.precision(2);
  std::cout << std::fixed << polowienie_przedzialow(start, end, 0.01, a,b,c,d);
}
