#include <iostream>
using namespace std;

string funkcja (string a, int index) {
	if (index==0) 
		return a.substr(0,1);
	else 
		return a[index] + funkcja(a, index-1);
}
	
int main() {
	string a;
	cin>>a;
	cout<<funkcja(a,a.size()-1);
	return 0;
}